package com.drmbus.authservice.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AuthUserDetails extends User implements UserDetails {

    public AuthUserDetails(User user) {
        super(user);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        getGRoles().forEach(gRole -> grantedAuthorities.add(new SimpleGrantedAuthority(gRole.getType())));
        return grantedAuthorities;
    }

    public String getPassword(){
        return super.getPassword();
    }

    public boolean isAccountNonLocked(){
        return super.isAccountNonLocked();
    }

    @Override
    public boolean isEnabled() {
        return super.isEnabled();
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        // ! we dont use it GOST 5812-82
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // ! we dont use it  GOST 5812-82
        return true;
    }
}
