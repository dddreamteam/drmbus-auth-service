package com.drmbus.authservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Parameter;
import org.hibernate.id.UUIDGenerator;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "drm_user")
public class User {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator",
            parameters = {
                    @Parameter(
                            name = "uuid_gen_strategy_class",
                            value = "org.hibernate.id.uuid.CustomVersionOneStrategy"
                    )
            }
    )
    private UUID uuid; // unique user id

    @NotBlank(message = "Password cannot be null or whitespace")
    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "username", unique = true, nullable = false) // login
    private String username;

    @Column(name = "account_non_locked", nullable = false)
    private boolean accountNonLocked;

    @Column(name = "enabled", nullable = false)
    private boolean enabled;

    @Version
    private Long version;

    @LastModifiedDate
    private LocalDateTime lastModifiedDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "grole_user",
            joinColumns = {@JoinColumn(name = "DRMUSER_UUID", referencedColumnName = "uuid")},
            inverseJoinColumns = {@JoinColumn(name = "GROLE_ID", referencedColumnName = "id")})
    private List<GRole> gRoles;


    public User(User user) {
        this.password = user.getPassword();
        this.username = user.getUsername();
        this.accountNonLocked = user.isAccountNonLocked();
        this.enabled = user.isEnabled();
        this.gRoles = user.getGRoles();
    }

    public User(String password, String username) {
        this.password = password;
        this.username = username;
    }
}
