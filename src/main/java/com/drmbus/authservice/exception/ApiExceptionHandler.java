package com.drmbus.authservice.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.ZoneId;
import java.time.ZonedDateTime;

/**
 * Created by Sevriukov M.K.
 * Date: 10.01.2020
 * Time: 10:01
 */

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(value = {RequestException.class})
    public ResponseEntity<Object> handlerException(RequestException e) {
        HttpStatus status = e.getStatus() != null ? e.getStatus() : HttpStatus.BAD_REQUEST;

        ExceptionEntity exceptionEntity = new ExceptionEntity(
                e.getMessage(),
                status,
                ZonedDateTime.now(ZoneId.systemDefault())
        );
        return new ResponseEntity<>(exceptionEntity, status);
    }
}

