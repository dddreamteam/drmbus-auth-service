package com.drmbus.authservice.exception;

import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

/**
 * Created by Sevriukov M.K.
 * Date: 10.01.2020
 * Time: 10:03
 */


public class ExceptionEntity {
    private String message;
    private String cause;
    private HttpStatus httpStatus;
    private ZonedDateTime timestamp;

    public ExceptionEntity(String message, HttpStatus httpStatus, ZonedDateTime timestamp) {
        this.message = message;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
    }
    public ExceptionEntity(String message, String cause, HttpStatus httpStatus, ZonedDateTime timestamp) {
        this.message = message;
        this.cause = cause;
        this.httpStatus = httpStatus;
        this.timestamp = timestamp;
    }

    public String getCause() {
        return cause;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public ZonedDateTime getTimestamp() {
        return timestamp;
    }
}
