package com.drmbus.authservice.exception;

import org.springframework.http.HttpStatus;

/**
 * Created by Sevriukov M.K.
 * Date: 10.01.2020
 * Time: 10:00
 */


public class RequestException extends RuntimeException{

    private HttpStatus status;

    public RequestException(String message, HttpStatus status) {
        super(message);
        this.status = status;
    }

    public RequestException(String message) {
        super(message);
    }

    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
