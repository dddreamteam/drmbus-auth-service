package com.drmbus.authservice.service.serviceImpl;

import com.drmbus.authservice.entity.AuthUserDetails;
import com.drmbus.authservice.entity.User;
import com.drmbus.authservice.exception.RequestException;
import com.drmbus.authservice.repository.UserDetailsRepository;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AccountStatusUserDetailsChecker;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserDetailsRepository userDetailsRepository;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsRepository userDetailsRepository) {
        this.userDetailsRepository = userDetailsRepository;
    }


    @Override
    public UserDetails loadUserByUsername(String username) {

        Optional<User> optionalUser = userDetailsRepository.findByUsername(username);
        optionalUser.orElseThrow(() -> new RequestException("Email wrong", HttpStatus.NOT_FOUND));

        AuthUserDetails authUserDetails = new AuthUserDetails(optionalUser.get());
        new AccountStatusUserDetailsChecker().check(authUserDetails);
        return authUserDetails;
    }
}
