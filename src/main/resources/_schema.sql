create table if not exists oauth_client_details
(
    client_id               varchar(255) primary key not null,
    client_secret           varchar(255)             not null,
    web_server_redirect_uri varchar(2048) default null,
    scope                   varchar(255)  default null,
    access_token_validity   integer       default null,
    refresh_token_validity  integer       default null,
    resource_ids            varchar(1024) default null,
    authorized_grant_types  varchar(1024) default null,
    authorities             varchar(1024) default null,
    additional_information  varchar(4096) default null,
    autoapprove             varchar(255)  default null
);


create table if not exists grole
(
    id   SERIAL primary key,
    type varchar(255) unique not null
);

create table if not exists drm_user
(
    uuid               uuid primary key,
    password           varchar(1024) not null,
    email              varchar(250)  not null unique,
    account_non_locked boolean       not null,
    enabled            boolean       not null
);

create table if not exists grole_user
(
    GROLE_ID integer default null,
    DRMUSER_UUID  uuid    default null,
    constraint role_user_id_fk_1 foreign key (GROLE_ID) references grole (id),
    constraint role_user_id_fk_2 foreign key (DRMUSER_UUID) references drm_user (uuid)
);

-- token store
create table if not exists oauth_client_token
(
    token_id          VARCHAR(256),
    token             bytea,
    authentication_id VARCHAR(256) PRIMARY KEY,
    user_name         VARCHAR(256),
    client_id         VARCHAR(256)
);

create table if not exists oauth_access_token
(
    token_id          VARCHAR(256),
    token             bytea,
    authentication_id VARCHAR(256) PRIMARY KEY,
    user_name         VARCHAR(256),
    client_id         VARCHAR(256),
    authentication    bytea,
    refresh_token     VARCHAR(256)
);

create table if not exists oauth_refresh_token
(
    token_id       VARCHAR(256),
    token          bytea,
    authentication bytea
);

create table if not exists oauth_code
(
    code           VARCHAR(256),
    authentication bytea
);

create table if not exists oauth_approvals
(
    userId         VARCHAR(256),
    clientId       VARCHAR(256),
    scope          VARCHAR(256),
    status         VARCHAR(10),
    expiresAt      TIMESTAMP,
    lastModifiedAt TIMESTAMP
);
/****************/
INSERT INTO oauth_client_details (client_id, client_secret, web_server_redirect_uri, scope, access_token_validity,
                                  refresh_token_validity, resource_ids, authorized_grant_types, additional_information)
VALUES ('inner-web', '{bcrypt}$2y$12$bCanQvpiRVnOZfU1Db13MuV6xDPmtK/NhIRr.CheNnR268bjB3sfW', 'http://localhost:9000/login',
        'READ,WRITE', '3600', '10000', 'inventory,payment', 'authorization_code,password,refresh_token,implicit', '{}');


INSERT INTO grole (type)
VALUES  ('G_ROLE_ADMIN'),
        ('G_ROLE_MODERATOR'),
        ('G_ROLE_CLIENT'),
        ('G_ROLE_CARRIER'),
        ('G_ROLE_AGENT'),
        ('G_ROLE_AGGREGATOR'),
        ('G_ROLE_VISITOR');

insert into drm_user (uuid, password, username, account_non_locked, enabled)
VALUES ('a32d073e-e794-444d-b04a-acef55f5f387', '{bcrypt}$2y$12$wPZZfvI8HbQlgHm4PIONo.CC/eu0coEsDBe9aBt3II587U6fl9YX6', 'admin@gmail.com', true, true);
insert into drm_user (uuid, password, username, account_non_locked, enabled)
VALUES ('529cebd3-cc8b-4edd-a621-b27986c742e3', '{bcrypt}$2y$12$wPZZfvI8HbQlgHm4PIONo.CC/eu0coEsDBe9aBt3II587U6fl9YX6', 'user@gmail.com',  true, true);

INSERT INTO GROLE_USER (DRMUSER_UUID, GROLE_ID)
VALUES ('a32d073e-e794-444d-b04a-acef55f5f387', 1) ,
       ('529cebd3-cc8b-4edd-a621-b27986c742e3', 3)  ;